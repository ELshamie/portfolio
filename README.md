# GitLab Portfolio
## About Me
My name is Said Elshamie and I'm a software developer specializing in Java and web development. I am currently an apprentice and would like to expand my experience in web application development and currently looking for new projects and challenges.
<br>
<hr>

## My first Springboot Project
My first Springboot project was a project I developed as part of my education. It is a web application that allows users to create and group participants & trainers | assign subjects. The application was developed in Java and uses the Spring framework. The project helped me deepen my knowledge of web development and become familiar with the Spring framework.
<br>
<hr>

## My first APP
My first APP was my first project in Kotlin as a developer. It's a simple Android app that allows users to create and save voice notes. I developed the app as part of my training and it uses internal storage. Although the app is very basic, it was an important milestone in my career as a developer as it helped me learn important Android development fundamentals.
<br>
<hr>

## Bombermaan AI
It's a recreation of a popular multiplayer game based on the classic game "Bomberman". I'm developing the game in Java and using the ejml library.
<br>
<hr>

## My personal strongpoints:

### Java

+ OOP
+ Algorithms
+ Spring Boot
+ REST
+ JPA
+ Hibernate

### Kotlin
+ Android Studio
+ Compose for desktop
+ JetpackCompose

### Project Management

+ Scrum
+ Requirements Engineering
+ Version Control via Gitlab

### Data Technology and System-management

+ ISO/OSI
+ IP's
+ Filesystems